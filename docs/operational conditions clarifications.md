# Gaia-X Labels Operational Conditions Clarifications


# Table of contents
1. [Scope of the document](#Scope_of_the_document)
2. [Scope of the labelling](#Scope_of_the_labelling)
3. [Owner of the label](#Owner_of_the_label)
4. [Principles for issuers of the labels](#Principles_for_issuers_of_the_labels)
5. [Methodology](#Methodology)
6. [Becoming an Evaluation Body (EvaB)](#Becoming_an_Evaluation_Body)
7. [Labels validity](#Labels_validity)
8. [Update cycles – material and rules of the game](#Update_cycles–material_and_rules_of_the_Game)
9. [Legal framework – use, revocation, maintenance of the label (issuer - labelled)](#Legal_Framework–Use_Revocation_Maintenance_of_the_label)
10. [Legal framework - Gaia-X and issuer](#Legal_Framework_Gaia-X_and_Issuer)
11. [IP](#IP)


## 1.Scope of the document <a name="Scope_of_the_document"></a>
text

## 2.Scope of the labelling <a name="Scope_of_the_labelling"></a>

#### i.Carve-out
text



## 3.Owner of the label <a name="Owner_of_the_label"></a>

Label Owners are entities that decide to define a specific Label for their business. These can be Service Providers, Service Users or Governmental Authorities, Standardisation Authorities, Trade Associations, Industrial Associations, etc. 



#### i.“Gaia-X” labels 
Gaia X association is owner of one label called “Gaix X label” ( name to be defined)  which has 3 levels

ii . Domain specific labels

Other entities that Gaia x can be owners of different labels they can define by adding some specific rules to the Gaia X label. For example, the financial sector could decide that they add some rules to the Gaia x label in order to have a financial label.

#### iii.Consistency of labels, inheritance and reciprocity of conformity
text

## 4.Principles for issuers of the "Gaia x label" <a name="Principles_for_issuers_of_the_labels"></a>
Label issuers are entities that are legally responsible for the maintenance of the labels for the delivering of the labels and that are monitoring the relations ships with the Cloud service providers.
3 choices are open :
-	First choice : Gaia x choice is to issue the labels and to do all the operational and maintenance work necessary to run these labels
-	Second choice  - Gaia x choice is to be owner of the label but chooses to completely delegate the operational and maintenance work to some entities that will issue the  Gaia x labels  on behalf of gaia x . How to become such an entity is described in part 6 of this document.
-	Third choice  - Gaia x choice is to be the owner of the label, to issue the level 1 label but to delegate level 2 and level 3 to some external entities. How to become such an entity is described in part 6 of this document.
-	The recommendation of the subworking group is the following : to be proposed by the labelling working group.


## 5.Methodology <a name="Methodology"></a>

#### i.Types of evaluation 

##### 1.Three types of evaluation 
text
##### 2.Retrospectiveness vs anticipating assurance 
text
##### 3.Management process vs individual assessments
text
##### 4.Sampling?  
text
##### 5.Complaints  
text

#### ii.Expected evaluation results and statements
text
#### iii.Decision to award a label

##### 1.Who takes the decision
text
##### 2.Based on what information 
text
##### 3.Integration of sub-services 
text
##### 4.How to deal with findings 
text

## 6.Becoming an Evaluation Body (EvaB) <a name="Becoming_an_Evaluation_Body"></a>

#### i.General material requirements

##### 1.legal entity, etc. 
text
##### 2.Expertise in subject matter 
text
##### 3.Impartiality
text
##### 4.Tracibility and transparency 
text
##### 5.Confidentiality 
text
##### 6.Comparability of findings 
text
##### 7.etc. 
text
##### 8.audit the auditor 
text

#### ii.Additional formal requirements

##### 1.Accreditation against specific evaluation standards
text
##### 2.Accreditation by specific national or European bodies (e.g., NCCA, Data protection authority, etc. ) 
text

## 7.Labels validity <a name="Labels_validity"></a>

#### i.Lifetime
text
#### ii.(Mandatory) prolonging
text
#### iii.Termination process
text

## 8.Update cycles – material and rules of the game <a name="Update_cycles–material_and_rules_of_the_Game"></a>

#### i.Minimum stability
text
#### ii.Transition periods
text
#### iii.Grace periods
text
#### iv.Consistency and scalability with other standards
text

## 9.Legal Framework – use, revocation, maintenance of the Label (issuer - labelled) <a name="Legal_Framework–Use_Revocation_Maintenance_of_the_label"></a>

#### i.General terms and conditions

##### 1.Re-affirmation of conformity 
text
##### 2.Acceptance of “rules of the game” 
text
##### 3.Establishment of “empowered” relation between Issuer and labelled 
text

#### ii.Form
text
#### iii.License agreement owner-issuer

##### 1.Possible IP registration 
text
##### 2.Regional scope (probably worldwide)
text

## 10.Legal framework - Gaia-X and issuer <a name="Legal_Framework_Gaia-X_and_Issuer"></a>

#### i.IP licensing
text
#### ii.“Chain” of powers
text
#### iii.“Chain” of rules of the game
text

## 11.IP <a name="IP"></a>

#### i.Export control
text
