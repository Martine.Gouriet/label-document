# Gaia-X Labelling Criteria

## Preface

The Gaia-X Labelling Criteria document links back to the [Gaia-X Labelling Framework](https://gaia-x.eu/sites/default/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf) paper which was published in November 2021. 

## Introduction

For Gaia-X to ensure a higher and unprecedented level of trust in digital platforms, we need to make trust an easy to understand and adopted principle. For this reason, Gaia-X developed a Trust Framework – formerly known as Gaia-X Compliance - and Labelling Framework that safeguards data protection, transparency, security, portability, and flexibility for the ecosystem as well as sovereignty and European Control.

The Trust Framework is the set of rules that define the minimum baseline to be part of the Gaia-X Ecosystem. Those rules ensure a common governance and the basic levels of interoperability across individual ecosystems while letting the users in full control of their choices.

In other words, the Gaia-X Ecosystem is the virtual set of participants and service offerings following the Gaia-X requirements from the Gaia-X Trust Framework.

The Trust Framework uses verifiable credentials and linked data representation to build a FAIR knowledge graph of verifiable claims from which additional trust and composability indexes can be automatically computed. 
The Labelling Framework is based on the Trust Framework (named compliance framework in former documents) based on self-descriptions. Thus, it is ensured that all information required to make a qualified choice between different services is available in a consistent and standardized machine-readable form. This Trust Framework is introduced in the [Gaia-X Architecture Document](https://gaia-x.eu/sites/default/files/2022-01/Gaia-X_Architecture_Document_2112.pdf), section 4.2. 

The Labelling Framework itself is further detailed and translated into concrete criteria and measures in the Gaia-X Labelling Criteria document. The criteria list brings together the policies and requirements from the committees – Policies and Rules Committee, Technical Committee, Data Spaces and Business Committee – along with comprehensive verification means to ensure that these requirements can be met. It allows for further differentiation between services that is necessary for users wanting to find services for different purposes and with different needs. It defines minimum qualification levels for the attributes described in the transparency framework.

However, it must be clarified that :
- Some of the rules are High Level Objectives and still need to be more detailed and specified to be implementable. The Policy Rules Committee of Gaia- X with its 3 sub working groups will work on it on further versions. The next version will define the frequency of the updates of this document.
- Redundancies are acknowledged. They shall be resolved to the extent possible in the future iterations. Some redundancies are a result of externalities, such as underlying standards, schemes, laws which cannot be resolved.
- Some of the criteria can be further detailed with the relevant acceptable standards, in that case they are identified. There will be a process to identify additional standards and maintain already listed standards, which will follow good practices defining objective criteria. This shall ensure both quality of accepted standards and neutral and fair access.

## Design Principles

The Gaia-X Labelling Framework introduced a set of core principles that are being refined by the criteria.

### Consistency among the Gaia-X ecosystem

Gaia-X Labels reflect the essence of our objectives and concepts. They represent the results of decisions and deliverables introduced by the various Gaia-X committees and approved by the Board of Directors. Hence, the following key principles for labelling are either directly adopted or derived from our main documents (i.e. the Gaia-X Architecture Document, the Gaia-X Policy Rules Document, or the Gaia-X Principles for Data Spaces) or have been widely adopted by the respective committees and will be published soon. Hence, the labelling criteria are always in line with the corresponding concepts and papers.

The references numbers of the documents are the following:
- Gaia-X policy rules document – PRD 2204
- Gaia-X Architecture document – TAD2112

### Scalability and extensibility

Based on the three basic labels further Gaia-X Labels can be created to fit new needs, in particular using extension profiles for country and domain specific requirements. Extension profiles can also leverage the labelling criteria by adding and defining on-top requirements for particular purposes. To ensure impact and consistency of Gaia-X Labels, new labels and extensions have to be authorized by the Gaia-X Association (Board of Directors).

### Composability and modularity

Gaia-X Labels are logical groupings of composable service attributes. This results in particular in the assignment of a common set of policies, technical requirements and data spaces criteria to one or multiple of three levels.
At the same time, Gaia-X labels base upon existing schemes, certifications, testates and approved codes of conduct where possible to allow reuse of established standards and thereby simplifying the process. Only in areas where no standard has been identified Gaia-X will introduce its own set of attributes and processes to verify the information given.

### Standards , self assessment and Conformity Assessment Bodies ( CAB)

Gaia-X labels do not reference text or standards which are not yet approved ( example the current proposal of the data act or the EUCS) but tries to align with this moving target. Whenever these standards are approved, Gaia-X will adapt its labels in accordance with these standards. The process to add these new standards will be detailed in a later version.

The verification of the adherence to label criteria can be through self-assessment or external Conformity Assessment Bodies (CAB) as defined later on this document.

Gaia-X Service Offerings are defined by Provider generated Self-Descriptions which include claims of adherence to the Labeling Criteria. The proof of a validation of a claim will be technically be realized through “W3C Verifiable Credentials”. The Verifiable Credential can either be issued by a provider or a CAB directly or it can be created by a trusted Verifiable Credential issuer based on existing documentation (like a signed PDF or paper document).

The Verifiable Credential includes the entity asserting validity of the claim; the list of trusted Verifiable Credentials issuers is maintained in the Gaia-X registry.

Users at any time can query the Self-Description of the Service Offering and for each claim extract the entity and the result of the assessment.

The process including the possible process of revoking trust to specific CAB or revocation of validity of Self-Description is described in the Gaia-X “Trust Framework”.

Conformity Assessment Bodies ( CAB): The Gaia- X associations reserves its right to choose its own CAB of its own three basic labels. A new detailed document will be issued on the process to choose the relevant CAB. Where the Labelling Framework lacks reference to accepted standards, Gaia-X will define a dedicated verification process including a process to appoint adequate a CAB (Conformity Assessment Body). Both processes will follow international recognized good practices, including impartiality, comparability, reliability and accessibility.

### Federation of Verification

Gaia-X labels are issued and verified in a federated manner. The concept of modularity also allows Gaia-X to reuse existing certifications for the underlying service attributes whenever possible, hence reducing the cost and complexity of embracing Gaia-X labelling, especially for existing, already certified, services. Verification processes defined by Gaia-X itself will also base on a federation of responsibilities.

### Further design principles

The modularity concept requires Gaia-X labelling criteria to describe rather high-level objectives as the detailed requirements are further described in the corresponding standards that are acknowledged. 
As of today, Gaia-X Labels are issued to a specific Service Offering unless stated otherwise. Only the criteria defined by the DSBC apply to data-sharing networks and define the governance, usage policies and obligations among ecosystem partners.

## Gaia-X Labelling Criteria

### Contractual governance

**Criterion 1 :  The Provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented.**

Source: PRD v2204, Chapter: 1.1.1

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 2 : The Provider shall have an option for each legally binding act to be governed by EU/EEA/Member State law.**

Source PRD v2204 chapter 1.1.2

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 3 : The Provider shall clearly identify for which parties the legal act is binding.**

Source PRD v2204 chapter 1.1.3

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 4 : The Provider shall ensure that the legally binding act covers the entire provision of the service offering**

Source PRD v2204 chapter 1.1.4

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

### Transparency

**Criterion 5 :  The Provider shall ensure there are specific provisions regarding service interruptions and business continuity (e.g., by means of a Service Level Agreement), provider's bankruptcy or any other reason by which the provider may cease to exist in law.**

Source PRD v2204 chapter 1.2.1

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 6 : The Provider shall ensure there are provisions governing the rights of the parties to use the service and any data therein.**

Source PRD v2204 chapter 1.2.2

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

An accepted standard is ISO19944

**Criterion 7 : The Provider shall ensure there are provisions governing changes, regardless of their kind.**

Source PRD v2204 chapter 1.2.3

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 8 : The Provider shall ensure there are provisions governing aspects regarding copyright or any other intellectual property rights.**

Source PRD v2204 chapter 1.2.4

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 9 : The Provider shall declare the general location of physicals Resources at urban area level.
Note: the urban area level is a geographical location more accurate than a country, province or region.**

Source PRD v2204 chapter 1.2.5

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 10 : The Provider shall explain how information about subcontractors and related data localization will be communicated.**

Note: this applies to the subcontractors essential to the provision of the Service Offering, including any sub-processors

Source PRD v2204 chapter 1.2.6

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 11 : The Provider shall communicate to the customer where the applicable jurisdiction(s) of subcontractors will be.**

Note: this applies to the subcontractors essential to the provision of the Service Offering, including any sub-processors

Source PRD v2204 chapter 1.2.7

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 12 : The Provider shall include in the contract the contact details where customer may address any queries regarding the Service Offering and the contract.**

Note: Queries include request during the pre-contractual state, before coming to an agreement.

Source PRD v2204 chapter 1.2.8

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 13 : The Provider shall adopt the Gaia-X Trust framework, by which Customers may verify Provider’s compliance.**

Source PRD v2204 chapter 1.2.9

Applicable to all levels

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: self-assessment through the trust framework

**Criterion 14 : Service Offering shall include a policy using a common Domain-Specific Language (DSL) to describe Permissions, Requirements and Constraints.**

Source: TAD v2112, Chapter: 4.1

Applicable to all levels

Verifying Entity: Gaia-X Compliance Service Provider

Verification Process: Gaia-X trust framework checking the self-description

**Criterion 15 : Service Offering requires being operated by service offering provider with a verified identity.**

Source: TAD v2112, Chapter: 4.2 / 4.3

Applicable to all levels

Verifying Entity: Gaia-X Compliance Service Provider

Verification Process: Gaia-X trust framework checking the self-description

**Criterion 16 : Service Offering must provide a conformant self-description.**

Source: TAD v2112, Chapter: 4.4 & 4.6.2

Applicable to all levels

Verifying Entity: Gaia-X Compliance Service Provider

Verification Process: Gaia-X trust framework checking the self-description

**Criterion 17 : Self-Description attributes need to be consistent across linked Self-Descriptions.**

Source: TAD v2112, Chapter: 4.4 & 4.6.2

Applicable to all levels

Verifying Entity: Gaia-X Compliance Service Provider

Verification Process: Gaia-X trust framework 

**Criterion 18 : Service Offering consumer needs to have a verified identity provided by the Federator**

Source: TAD v2112, Chapter: 4.4.1

Applicable to all levels

Verifying Entity: Gaia-X Compliance Service Provider

Verification Process: Gaia-X trust framework checking the self-description

### Data Protection

**Criterion 19 : The Provider shall offer the ability to establish a contract under Union or EU/EEA/Member State law and specifically addressing GDPR requirements.**

Source PRD v2204 chapter 2.1.1

Verifying Entity: L1: Gaia-X AISBL or mandated entity

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 20 : The Provider shall define the roles and responsibilities of each party.**

Note: This considers the roles and responsibilities of the parties involved in the scope of this Service Offering.

Source: PRD v2204, Chapter: 2.1.2

Verifying Entity: L1: Gaia-X AISBL or mandated entity

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 21 :  The Provider shall clearly define the technical and organizational measures in accordance with the roles and responsibilities of the parties, including an adequate level of detail.**

Source: PRD v2204, Chapter: 2.1.3

Verifying Entity: L1: Gaia-X AISBL or mandated entity 

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 22 :  The Provider shall be ultimately bound to instructions of the Customer.**

Source: PRD v2204, Chapter: 2.2.1

Verifying Entity: L1: Gaia-X AISBL or mandated entity 
L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 23 : The Provider shall clearly define how customer may instruct, including by electronic means such as configuration tools or APIs.**

Source: PRD v2204, Chapter: 2.2.2

Verifying Entity: L1: Gaia-X AISBL or mandated entity 

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration 

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 24 : The Provider shall clearly define if and to which extent third country transfer will take place. 
Source: PRD v2204, Chapter: 2.2.3**

Verifying Entity: 

L1: Gaia-X AISBL or mandated entity 

L2: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration 

L2  CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

This rule is not applicable to level 3

**Criterion 25 : The Provider shall clearly define if and to the extent third country transfers will take place, and by which means of Chapter V GDPR these transfers will be protected.**

Source : PRDv2204 , chapter 2.2.4

Verifying Entity:

L1:  Gaia-X AISBL or mandated entity 

L2: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: 

L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration 

L2 : CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 
Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

This rule is not applicable for level 3

**Criterion 26  : The Provider shall clearly define if and to which extent sub-processors will be involved.**

Source: PRD v2204, Chapter: 2.2.5

Verifying Entity: 

L1:  Gaia-X AISBL or mandated entity 

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: 

L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration 

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 27 : The Provider shall clearly define if and to the extent sub-processors will be involved, and the measures that are in place regarding sub-processors management.**

Source: PRD v2204, Chapter: 2.2.6

Verifying Entity: 

L1: Gaia-X AISBL or mandated entity 

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration 

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 28 : The Provider shall define the audit rights for the Customer.**

Source: PRD v2204, Chapter: 2.2.7

Verifying Entity: 

L1: Gaia-X AISBL or mandated entity 

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 29 : In case of a joint controllership, the Provider shall ensure an arrangement pursuant to Art. 26 (1) GDPR is in place.**

Source: PRD v2204, Chapter: 2.3.1

Verifying Entity:

L1: Gaia-X AISBL or mandated entity

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration 

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 30 :  In case of a joint controllership, at a minimum, the Provider shall ensure that the very essence of such agreement is communicated to data subjects.**

Source: PRD v2204, Chapter: 2.3.2

Verifying Entity: 

L1: Gaia-X AISBL or mandated entity

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/

Validation based on audit by CAB 
Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

**Criterion 31 : In case of a joint controllership, the Provider shall publish a point of contact for data subjects.**

Source: PRD v2204, Chapter: 2.3.3

Verifying Entity: 

L1: Gaia-X AISBL or mandated entity 

L2/L3: CoC Art. 40: Competent authority accredited monitoring body or third party; Certification: Accredited CAB (ISO 17065) 

Verification Process: L1: self-verified through internal audit according to an approved CoC/Certification scheme and signed Gaia-X Self-Declaration 

L2 / L3: CoC (Art. 40): Evaluation by monitoring or third party; Certification (Art. 42): Inspection/Verification/Validation based on audit by CAB

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

### Security

For all the security requirements, the criteria follow as much as possible the current discussions on the European Cloud Scheme (EUCS) . When the EUCS is finalized, Gaia -X will adapt consequently these criteria. Therefore  the terms on the different criteria on this item should be read in the light of EUCS

**Criterion 32 : Organization of information security: Plan, implement, maintain and continuously improve the information security framework within the organisation.**

Source: PRD v2204, Chapter: 3.1.1

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 33 : Information Security Policies: Provide a global information security policy, derived into policies and procedures regarding security requirements and to support business requirements**

Source: PRD v2204, Chapter: 3.1.2

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 34 : Risk Management: Ensure that risks related to information security are properly identified, assessed, and treated, and that the residual risk is acceptable to the CSP.**

Source: PRD v2204, Chapter: 3.3.3

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 35 : Human Resources: Ensure that employees understand their responsibilities, are aware of their responsibilities with regard to information security, and that the organisation's assets are protected in the event of changes in responsibilities or termination.**

Source: PRD v2204, Chapter: 3.3.4

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 36 : Asset Management: Identify the organisation's own assets and ensure an appropriate level of protection throughout their lifecycle.**

Source: PRD v2204, Chapter: 3.3. 5   

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching : C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 37 : Physical Security: Prevent unauthorised physical access and protect against theft, damage, loss and outage of operations.**

Source: PRD v2204, Chapter: 3.3. 6

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 38 : Operational Security: Ensure proper and regular operation, including appropriate measures for planning and monitoring capacity, protection against malware, logging and monitoring events, and dealing with vulnerabilities, malfunctions and failures.**

Source: PRD v2204, Chapter: 3.3.7

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following  recognized standards and/or good practices 

L2: onsite assessment following assessment process  according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 39 : Identity, Authentication and access control management: Limit access to information and information processing facilities.**

Source: PRD v2204, Chapter: 3.3. 8

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 40 : Cryptography and Key management: Ensure appropriate and effective use of cryptography to protect the confidentiality, authenticity or integrity of information.**

Source: PRD v2204, Chapter: 3.3.9

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 41 : Communication Security: Ensure the protection of information in networks and the corresponding information processing systems.**

Source: PRD v2204, Chapter: 3.3. 10

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 42 : Portability and Interoperability: Enable the ability to access the cloud service via other cloud services or IT systems of the cloud customers, to obtain the stored data at the end of the contractual relationship and to securely delete it from the Cloud Service Provider.**

Remark: this objective should be understood in the context of cybersecurity. Further portability objectives are defined in criteria 52 and 53

Source: PRD v2204, Chapter: 3.3.11

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 43 : Change and Configuration Management: Ensure that changes and configuration actions to information systems guarantee the security of the delivered cloud service.**

Source: PRD v2204, Chapter: 3.3.12

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 44 :  Development of Information systems: Ensure information security in the development cycle of information systems.**

Source: PRD v2204, Chapter: 3.3.13

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 45 : Procurement Management: Ensure the protection of information that suppliers of the CSP can access and monitor the agreed services and security requirements.**

Source: PRD v2204, Chapter: 3.3.14

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 46 : Incident Management: Ensure a consistent and comprehensive approach to the capture, assessment, communication and escalation of security incidents.**

Source: PRD v2204, Chapter: 3.3. 15

Assessing  Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS  Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 47 : Business Continuity: Plan, implement, maintain and test procedures and measures for business continuity and emergency management.**


Source: PRD v2204, Chapter: 3.3.16

This criterion is consistent with criterion 60 ( chapter European control)

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Level 2
Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 48 : Compliance: Avoid non-compliance with legal, regulatory, self-imposed or contractual
information security and compliance requirements.**

Source: PRD v2204, Chapter: 3.3.17

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 49 : User documentation: Provide up-to-date information on the secure configuration and known vulnerabilities of the cloud service for cloud customers.**

Source: PRD v2204, Chapter: 3.3.18

Assessing Entity: 

L1: internal + authorized entity according to the EUCS  Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 50 : Dealing with information requests from government agencies: Ensure appropriate handling of government investigation requests for legal review, information to cloud customers, and limitation of access to or disclosure of data.**

Source: PRD v2204, Chapter: 3.3.19

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process  according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion 51 :  Product safety and security: Provide appropriate mechanisms for cloud customers.
Source: PRD v2204, Chapter: 3.3.20.**

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices  

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

### Portability

**Criterion 52 : The Provider shall implement practices for facilitating the switching of Providers and the porting of data in a structured, commonly used and machine-readable format including open standard formats where required or requested by the Provider receiving the data.**

Note: The Customer can act as an intermediary for transferring data between Providers, e.g. by executing the provided tools to execute the transfer.

Note: The data received by the Customer or the importing Provider could include configuration information as well as information about the software systems used for the Service Offering.

Source: PRD v2204, Chapter: 4.1.1

Verifying Entity: 

L1 & L2: Gaia-X AISBL or mandated entity 

L3: SWIPO-accredited CAB 

Verification Process:

L1 & L2: self-verified through internal audit and signed Gaia-X Self-Declaration 

L3: SWIPO self-declaration

Accepted Standards: SWIPO IaaS, SaaS and merged code CoC

**Criterion 53 : The Provider shall ensure pre-contractual information exists, with sufficiently detailed, clear and transparent information regarding the processes of data portability, technical requirements, timeframes and charges that apply in case a professional user wants to switch to another Provider or port data back to its own IT systems.**

Source: PRD v2204, Chapter: 4.1.2

Verifying Entity: 

L1 & L2: Gaia-X AISBL or mandated entity 

L3: SWIPO-accredited CAB 

Verification Process: 

L1 & L2: self-verified through internal audit and signed Gaia-X Self-Declaration 

L3: SWIPO self-declaration (M)

Accepted Standards: SWIPO IaaS, SaaS merged code CoC

### European Control

**Criterion 54 :  For Label Level 2, the Provider shall provide the option that all data are processed and stored exclusively in EU/EEA.**

Source: PRD v2204, Chapter: 5.1.1

This criterion is only required for level 2

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration through the trust framework

Accepted Standards: -

**Criterion 55 :  For Label Level 3, the Provider shall process and store all data exclusively in the EU/EEA.**

Source: PRD v2204, Chapter: 5.1.2

This criterion is only required for level 3

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration through the trust framework

Accepted Standards: -

**Criterion 56 : For Label Level 3, where the Provider or subcontractor is subject to legal obligations to transmit or disclose data on the basis of a non-EU/EEA statutory order, the Provider shall have verified safeguards in place to ensure that any access request is compliant with EU/EEA/Member State law.**

Source PRD 2204 chapter 5.1.3

Note – the safeguards are specified in criteria 57 to 60

Source: PRD v2204, Chapter: 5.1.3

This criterion is only required for level 3

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration 

Accepted Standards: 

**Criterion 57 : For Label Level 3, the Provider’s registered head office, headquarters and main establishment shall be established in a Member State of the EU/EEA.**

Source PRD 2204 chapter 5.1.4

This criterion is required only for level 3

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration

Accepted Standards: -

**Criterion 58 : For label level 3, Shareholders in the provider, whose registered head office, headquarters and main establishment are not established in a Member State of the EU shall not, directly or indirectly, individually or jointly, hold control of the CSP. Control is defined as the ability of a natural or legal person to exercise decisive influence directly or indirectly on the CSP through one or more intermediate entities, de jure or de facto. (cf. Council Regulation No 139/2004 and Commission Consolidated Jurisdictional Notice under Council Regulation (EC) No 139/2004 for illustrations of decisive control).**

Source PRD2204, chapter 5.1.5

This criterion is required only for level 3

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration

Accepted Standards: -

**Criterion 59 :  For Label Level 3, in the event of recourse by the Provider, in the context of the services provided to the Customer, to the services of a third-party company - including a subcontractor - whose registered head office, headquarters and main establishment is outside of the European Union or who is owned or controlled directly or indirectly by another third-party company registered outside the EU/EEA, the third-party company shall have no access over the Customer data nor access and identity management for the services provided to the Customer. The Provider, including any of its sub-processor, shall push back any request received from non-European authorities to obtain communication of personal data relating to European Customers, except if request is made in execution of a court judgment or order that is valid and compliant under Union law and applicable member states law as provided by Article 48 GDPR.**

Source:  PRD2204, chapter 5.1.6

This criterion is required only for level 3 

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration

Accepted Standards: -

**Criterion 60 :  For Label Level 3, the Provider must guarantee continuous autonomy for all or part of the services it provides. The concept of operating autonomy shall be understood as the ability to maintain the provision of the cloud computing service by drawing on the provider’s own skills or by using adequate alternatives**

Source:  PRD2204 chapter 5.1.7

This criterion is only required for level 3 

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration

Accepted Standards: -

**Criterion 61 : The Provider shall not access customer data unless authorized by the customer or when the access is in accordance with EU/EEA/Member State law.**

Source : PRD2204 chapter 5.2.1

This criterion is required for all 3 levels

Verifying Entity: Gaia-X AISBL or mandated entity

Verification Process: Gaia-X Self-Declaration

Accepted Standards: -

### Data Protection in Data Spaces

*All these criteria will be detailed in a further version of this document*
